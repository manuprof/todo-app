// creo un oggetto app vue
var app = Vue.createApp({
    // lifecycle
    // `mounted` viene eseguito ogni volta che carica questa classe
    async mounted() {
        // chiamo il metodo reloadTodos che si occupa
        // di caricare l'elenco dei todo ed assegnarlo
        // alla proprietà "todos"
        this.loading = true; // imposto il loading solo al mount
        this.reloadTodos();
    },
    methods: {
        async reloadTodos() {
            var todosResponse = await fetch('http://localhost:8000/api/todos');
            var todosJson = await todosResponse.json();
            // assegno al "data" todos l'array di todo 
            // che viene tornato dalla fetch
            this.todos = todosJson;
            this.loading = false;
        },
        // questo viene chiamato da @click "aggiungi"
        async addTodo() {
            await fetch('http://localhost:8000/api/todos', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    description: this.newTodoDescription
                })
            });
            // ricarico la lista dei todos
            this.reloadTodos();
            this.newTodoDescription = ''; // reset della casella di testo
        },
        async deleteTodo(idTodo) {
            //api/todos/{id} [DELETE]
            await fetch('http://localhost:8000/api/todos/' + idTodo, {
                method: 'DELETE',
                headers: { 'Content-Type': 'application/json' }
            });
            // RICARICO
            this.reloadTodos();
        },
        /** questo metodo serve a cambiare il campo done del todo nel DB
         *  quindi se done=1 deve mettere 0 e viceversa
         */
        async toggleTodo(todo){
             //api/todos/{id} [PUT]
             await fetch('http://localhost:8000/api/todos/' + todo.id, {
                method: 'PUT',
                headers: { 'Content-Type': 'application/json' },
                body:JSON.stringify({
                    description: todo.description,
                    done: todo.done == 1 ? 0 : 1 // cambio il valore di done
                })
            });
            // NON RICARICO 
            // OPTIMISTIC UPDATE 
            // spero che vada sempre a buon fine
            this.reloadTodos();
        }
    },
    template: /** alt + 96 (del tastierino) */ `
        <h1>TODO APP</h1>
        <div>
            <input type="text" v-model="newTodoDescription" />
            <button @click="addTodo">Aggiungi</button>
        </div>
        <svg v-if="loading" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin:auto;background:#fff;display:block;" width="70px" height="70px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
        <circle cx="50" cy="50" fill="none" stroke="#e15b64" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138">
          <animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" dur="1s" values="0 50 50;360 50 50" keyTimes="0;1"></animateTransform>
        </circle>
        </svg>
        <div v-if="todos.length <= 0 && loading === false">Non ci sono todos</div>
        <ul>
            <li v-for="todo in todos">
                <i class="mdi mdi-delete" @click="deleteTodo(todo.id)"></i>
                <input type="checkbox" :checked="todo.done===1" @click="toggleTodo(todo)" />
                <span>{{ todo.description }}</span>
            </li>
        </ul>
    `,
    data() {
        return {
            // questa proprietà la collego al valore dell'input
            newTodoDescription: '', // data-bind con l'input type=text
            loading: false,
            todos: [] // => new Array()
        }
    }
});
// gli dice di renderizzare tutto dentro il tag div#app
app.mount('#app');